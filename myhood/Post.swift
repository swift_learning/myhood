//
//  Posts.swift
//  myhood
//
//  Created by iulian david on 11/12/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import Foundation
import UIKit

class Post: NSObject, NSCoding {
    private var _imagePath: String!
    private var _title: String!
    private var _postDesc: String!
    
    init(imagePath: String, title: String, description: String){
        self._imagePath = imagePath
        self._title = title
        _postDesc = description
    }
    
    
    var imagePath: String {
        get{
            return _imagePath
        }
    }
    
    var title: String {
        get {
            return _title
        }
    }
    
    var postDesc: String {
        get {
            return _postDesc
        }
    }
    
    
    override init(){
        
    }
    
    required convenience init?(coder aDecoder: NSCoder){
        self.init()
        self._imagePath = (aDecoder.decodeObject(forKey: "imagePath") as? String)
        self._title = (aDecoder.decodeObject(forKey: "title") as? String)
        self._postDesc = (aDecoder.decodeObject(forKey: "postDesc") as? String)
    }
    
    func  encode(with aCoder: NSCoder) {
        aCoder.encode(self._imagePath, forKey: "imagePath")
        aCoder.encode(self._title, forKey: "title")
        aCoder.encode(self._postDesc, forKey: "postDesc")
    }
}
