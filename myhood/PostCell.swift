//
//  PostCell.swift
//  myhood
//
//  Created by iulian david on 11/12/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import UIKit

//The definition of the cell
class PostCell: UITableViewCell {

    @IBOutlet weak var postImage: UIImageView!
    
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var textLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        postImage.layer.cornerRadius = postImage.frame.size.width/2
        postImage.clipsToBounds = true
    }
    
   
    func configureCell(post: Post){
        titleLbl.text = post.title
        
        textLbl.text = post.postDesc
        
        let fullImgPath = DataService.instance.imageForPath(path: post.imagePath)
        postImage.image = fullImgPath
    }
    


}
