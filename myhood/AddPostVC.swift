//
//  AddPostVC.swift
//  myhood
//
//  Created by iulian david on 11/12/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import UIKit

//For using UIImagePickerControllerDelegate 
//WE NEED TO USE  UINavigationControllerDelegate
class AddPostVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var postImage: UIImageView!
    
    @IBOutlet weak var titleTxt: UITextField!
    
    @IBOutlet weak var descText: UITextField!
    
    //To pic an image we need to set up a UIImagePickerController
    var imagePicker: UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //make it rounder
        postImage.layer.cornerRadius = postImage.frame.size.width / 2
        postImage.clipsToBounds = true
        
        //initialize picker controller
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
    }
    
    
    @IBAction func makePost(_ sender: Any) {
        if let title = titleTxt.text, let desc = descText.text, let img = postImage.image {
            let imgPath = DataService.instance.saveImageAndCreatePath(image: img)
            let post = Post(imagePath: imgPath, title: title, description: desc)
            DataService.instance.addPost(post: post)
            dismiss(animated: true, completion: nil)
        }
    }
    
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func addPictue(_ sender: UIButton!) {
        sender.setTitle("", for: .normal)
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage  else {
            print("Something went wrong")
            return
        }
        postImage.image = image
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
   
}
