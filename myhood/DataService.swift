//
//  DataService.swift
//  myhood
//
//  Created by iulian david on 11/12/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

//Singleton that stores the Post values
//It will be used also to retrieve the data from
import Foundation
import UIKit

class DataService{
    
    static let KEY_POST = "posts"
    
    //The singleton instance
    static let instance = DataService()
    
    //the posts array used
    private var _loadedPosts = [Post]()
    
    var loadedPosts: [Post] {
        return _loadedPosts
    }
    
    //saving the posts to disk unsing NsKeyedArchiver
    func savePosts(){
        let postData = NSKeyedArchiver.archivedData(withRootObject: _loadedPosts)
        UserDefaults.standard.set(postData, forKey: DataService.KEY_POST)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "postsLoaded"), object: nil)
        UserDefaults.standard.synchronize()
    }
    
    //loading the posts from disk
    func loadPosts() {
        if let data = UserDefaults.standard.data(forKey: DataService.KEY_POST)! as? Data {
            if let postsArray = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Post] {
                 _loadedPosts = postsArray
            }
       
        }
    }
    
    func saveImageAndCreatePath(image: UIImage) -> String {
        let imgData = UIImagePNGRepresentation(image)
        let imgPath = "image\(Date.timeIntervalSinceReferenceDate).png"
        let fullPath = documentsPathForFileName(name: imgPath)
        do {
        try imgData?.write(to: URL(fileURLWithPath: fullPath), options: .atomic)
        } catch {
            print(error)
        }
        return imgPath
    }
    
    func imageForPath(path: String) -> UIImage? {
        
        let fullPath = documentsPathForFileName(name: path)
        let image = UIImage(named: fullPath)
        return image
    }
    
    
    func addPost(post: Post) {
        _loadedPosts.append(post)
        savePosts()
        loadPosts()
    }
    
    //for storing documents we need to save it to iOS 
    // documents directory
    // This is the function
    func documentsPathForFileName(name: String) -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let fullPath = paths[0] as NSString
        return fullPath.appendingPathComponent(name)
    }
    
}

extension String {
    func appendingPathComponent(_ string: String) -> String {
        return URL(fileURLWithPath: self).appendingPathComponent(string).path
    }
}
